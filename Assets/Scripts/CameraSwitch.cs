﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSwitch : MonoBehaviour {

    public Camera[] cameras;
    int currentCamera = 0;
    public Text changeInfo;

	// Use this for initialization
	void Start () {
        
        foreach (Camera camera in cameras)
        {
            camera.enabled = false;
        }
        cameras[currentCamera].enabled = true;
	}

    void UpdateCursorLockState()
    {
        if (Camera.current.name == "Main Camera")
        {
            Cursor.lockState = CursorLockMode.Confined;
        } else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab))
        {
            cameras[currentCamera].enabled = false;
            currentCamera++;
            currentCamera %= cameras.Length;
            cameras[currentCamera].enabled = true;
            changeInfo.text = "Camera: " + (currentCamera + 1);
            if(Camera.current.name == "Main Camera")
            {
                Cursor.visible = false;
            } else
            {
                Cursor.visible = true;

            }
            UpdateCursorLockState();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
	}
}
