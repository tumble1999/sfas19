﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITextTimeoutFade : MonoBehaviour {

    public bool startShown;
    float lifetime = 0.0f;
    public float startLifetime = 100.0f;
    string lastText;

	// Use this for initialization
	void Start ()
    {
        Text text = GetComponent<Text>();
        lastText = text.text;
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0.0f);
        if (startShown)
        {
            lifetime = startLifetime;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        Text text = GetComponent<Text>();
        if (lifetime <= 0)
        {
            lifetime = 0.0f;
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a-0.04f);
        } else
        {
            lifetime--;
            text.color = new Color(text.color.r, text.color.g, text.color.b, 1.0f);
        }
        if (lastText != text.text)
        {
            lastText = text.text;
            lifetime = startLifetime;
        }
	}
}
