﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {

    public Transform firstPersonPos;
    public Transform thirdPersonPos;
    public Transform thirdPersonFixedPos;
    public float zoomPercent = 1;

    // Use this for initialization
    void Start () {
        
	}

    Vector3 GetFirstToThird()
    {
        Vector3 firstToThird = thirdPersonPos.position - firstPersonPos.position;
        return firstToThird;
    }

    Vector3 GetDynamicToFixed()
    {
        Vector3 fullTravel = thirdPersonFixedPos.position - firstPersonPos.position;
        Vector3 dynamicToFixed = Vector3.Normalize(fullTravel) * (Vector3.Magnitude(fullTravel) - Vector3.Magnitude(GetFirstToThird()));
        return dynamicToFixed;
    }

    float GetFullDistance()
    {
        float fullDistance = Vector3.Magnitude(GetFirstToThird()) + Vector3.Magnitude(GetDynamicToFixed());
        return fullDistance;
    }
    float GetDynamicToFixedPercent()
    {
        return Vector3.Magnitude(GetFirstToThird()) / GetFullDistance();
        //return 1.0f;
    }

    Vector3 GetCameraPos()
    {
        Vector3 pos;
        if (zoomPercent <= GetDynamicToFixedPercent())
        {
            float percent = zoomPercent/GetDynamicToFixedPercent();
            float magnitude = percent * Vector3.Magnitude(GetFirstToThird());
            pos = Vector3.Normalize(GetFirstToThird()) * magnitude;
            pos += firstPersonPos.position;
        } else
        {
            float percent = zoomPercent;
            float magnitude = percent * Vector3.Magnitude(GetDynamicToFixed());
            pos = Vector3.Normalize(GetDynamicToFixed()) * magnitude;
            //pos += firstPersonPos.position;
        }
        /*else if (zoomPercent < GetDynamicToFixedPercent())
        {
            float percent = zoomPercent / GetDynamicToFixedPercent();
            float magnitude = percent * GetFullDistance();
            pos = Vector3.Normalize(GetFirstToThird()) * magnitude;
        }
        else
        {
            pos = thirdPersonFixedPos.position;
        }*/
        return pos;
    }

    // Update is called once per frame
    void Update() {
        if (zoomPercent < 0.0f)
        {
            zoomPercent = 0.0f;
        }
        if (zoomPercent>1.0f)
        {
            zoomPercent = 1.0f;
        }

        zoomPercent-= Input.mouseScrollDelta.y/40;
        transform.position = GetCameraPos();
	}
}
