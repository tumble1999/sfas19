﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {
    int level = 0;
    public float time = 20;
    public Transform ground;
    public Text timeIndicator;
    public Text levelIndicator;
    public Text enemyCountIndicator;
    public Text[] showOnGameEnd;
    public Text[] hideOnGameEnd;
    public GameObject[] spawnable;
    public GameObject player;
    public float enemyY;

    List<GameObject> enemies = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
	}

    int GetEnemyCount(int levelNum)
    {
        int x = levelNum;
        float enemyCountf = 4 + Mathf.Sin(x * Mathf.PI / 2) + x * x;
        int enemyCount = Mathf.RoundToInt(enemyCountf);
        return enemyCount;
    }

    Vector2 GetRandomEnemyPosition(Vector2 bounds)
    {
        float x = Random.value * bounds.x;
        float y = Random.value * bounds.y;

        Vector2 pos = new Vector2(x, y);

        return pos;
    }

    Vector2 GetLevelOrigin()
    {
        Vector3 levelOriginV3 = ground.position - ground.localScale / 2;
        Vector2 levelOriginV2 = new Vector2(levelOriginV3.x, levelOriginV3.z);
        return levelOriginV2;
    }

    Vector2 GetFloorBounds()
    {
        Vector3 boundsV3 = ground.localScale;
        Vector2 boundsV2 = new Vector2(boundsV3.x, boundsV3.z);
        return boundsV2;
    }

    void GenerateLevel()
    {
        int enemyCount = GetEnemyCount(level);
        while (enemies.Count != enemyCount)
        {
            Vector2 floorBounds = GetFloorBounds();
            Vector2 enemyPosXZ = GetLevelOrigin() + GetRandomEnemyPosition(floorBounds);
            Vector3 enemyPos = new Vector3(enemyPosXZ.x, enemyY, enemyPosXZ.y);
            GameObject prefab = spawnable[Mathf.RoundToInt(Random.value * (spawnable.Length - 1))];
            GameObject enemy = Instantiate(prefab, enemyPos, prefab.transform.rotation, transform);
            if (prefab.name.Contains("Enemy"))
            {
                enemy.name = "Enemy " + (enemies.Count + 1);
                enemies.Add(enemy);
            }
        }
        enemyCountIndicator.text = enemies.Count + " Enemies";
    }

    void NextLevel()
    {
        level++;
        time += 20;
    }

    void RemoveEnemy(GameObject enemy)
    {
        enemies.Remove(enemy);
    }

    void CheckEnemiesExistance()
    {
        for (int i = 0; i < enemies.Count; i++)
        {


            if (enemies[i] == null)
            {
                RemoveEnemy(enemies[i]);
                if (time > 0)
                {
                    GunLogic gunLogic = player.GetComponentInChildren<GunLogic>();
                    if (gunLogic)
                    {
                        gunLogic.AddAmmo(10, 1);
                        time += 5;
                    }
                }
            } else {
                if (enemies[i].transform.position.y < -3 || enemies[i].transform.position.y > 10)
                {
                    GameObject.Destroy(enemies[i]);
                }
            CheckEnemiesExistance();

            }
        }
    }

    void KillAll()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            GameObject.Destroy(enemies[i]);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        time -= Time.deltaTime;
        timeIndicator.text = "Time: " + (int)time;

        if (time <= 0.0f)
        {

            for (int i = 0; i < hideOnGameEnd.Length; i++)
            {
                Text text = hideOnGameEnd[i];
                text.color = new Color(text.color.r, text.color.g, text.color.b, 0.0f);
            }

            for (int i = 0; i < showOnGameEnd.Length; i++)
            {
                Text text = showOnGameEnd[i];
                text.color = new Color(text.color.r, text.color.g, text.color.b, 1.0f);
            }
            KillAll();
            return;
        }
        CheckEnemiesExistance();

        if (Input.GetKeyDown(KeyCode.K))
        {
            KillAll();
            CheckEnemiesExistance();

        }

        levelIndicator.text = "Level: " + level;

        if (enemies.Count != GetEnemyCount(level))
        {
            enemyCountIndicator.text = enemies.Count + " enemies left";
        }

        if (time!=0 && enemies.Count == 0)
        {
            NextLevel();
            GenerateLevel();
        }
    }
}
