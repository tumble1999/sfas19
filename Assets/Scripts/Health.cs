﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    // The total health of this unit
    [SerializeField]
    int m_Health = 100;
    [SerializeField]
    GameObject m_DamageIndicatorPrefab;
    [SerializeField]
    Transform m_DamageIndicatorSpawnPoint;

    public void DoDamage(int damage)
    {
        m_Health -= damage;
        m_DamageIndicatorPrefab.GetComponent<Damage>().value = -damage;
        Instantiate(m_DamageIndicatorPrefab,m_DamageIndicatorSpawnPoint.position,new Quaternion(0,0,0,0));
        if (m_Health < 0)
        {
            Destroy(gameObject);
        }
    }

    public bool IsAlive()
    {
        return m_Health > 0;
    }

    public int GetHealth()
    {
        return m_Health;
    }
}
