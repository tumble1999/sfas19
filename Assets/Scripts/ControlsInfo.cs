﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct Control
{
    public KeyCode key;
    public string infoText;
    [HideInInspector]
    public GameObject gameObject;
}

public class ControlsInfo : MonoBehaviour {
    public List<Control> controls;
    public GameObject textPrefab;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < controls.Count; i++)
        {
            Control control = controls[i];
            GameObject textObject = Instantiate(textPrefab, transform);

            Vector3 pos = textObject.GetComponent<RectTransform>().position;
            textObject.GetComponent<RectTransform>().position = new Vector3(pos.x,(i * 75.0f),pos.z);

            textObject.GetComponent<Text>().text = control.infoText;

            control.gameObject = textObject;
            controls[i] = control;
        }
	}

    void UpdatePositions()
    {
        for (int i = 0; i < controls.Count; i++)
        {
            Control control = controls[i];
            GameObject textObject = control.gameObject;

            Vector3 pos = textObject.GetComponent<RectTransform>().position;
            textObject.GetComponent<RectTransform>().position = new Vector3(pos.x, (i * 75.0f), pos.z);

            controls[i] = control;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        //Control[] tmpControls = controls;
        //controls = new Control[] { };
        for (int i = 0; i < controls.Count; i++)
        {
            Control control = controls[i];
            if (Input.GetKeyDown(control.key))
            {
                GameObject textObject = control.gameObject;
                Color color = textObject.GetComponent<Text>().color;
                GameObject.Destroy(textObject);
                controls.Remove(control);
            }
            //else { 
            //   controls[controls.Length] = control;
            //}
        }
        //controls = tmpControls;
        UpdatePositions();
    }
}
