﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Damage : MonoBehaviour {
    public int lifetime = 100;
    public int value = 0;
	// Use this for initialization
	void Start () {
        var tm = gameObject.GetComponent<TextMesh>();
        tm.text = value.ToString();
        if (value<0)
        {
            tm.color = new Color(1,0,0);
        }else
        {
            tm.color = new Color(0, 1, 0);
        }
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward,Camera.main.transform.rotation * Vector3.up);
        if (lifetime==0)
        {
            GameObject.Destroy(gameObject);
            return;
        }
        gameObject.transform.Translate(new Vector3(0,0.01f,0));
        lifetime--;
	}
}
